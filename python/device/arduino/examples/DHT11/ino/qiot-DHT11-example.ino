#include "DHT.h"
#include <Bridge.h>

#define DHTPIN 8

#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Bridge.begin();
  Serial.begin(9600); 
  dht.begin();

}
void loop() {
  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature();
  
  if (isnan(temperature) || isnan(humidity)) {
    Serial.println("Failed to read from DHT");
  } else {
    Serial.print("Humidity: "); 
    Serial.print(humidity);
    Serial.print(" %\t");
    Serial.print("Temperature: "); 
    Serial.print(temperature);
    Serial.println(" *C");

    Bridge.put("humidity", String(humidity));
    Bridge.put("temperature", String(temperature));
  }

   delay(1000);
}
