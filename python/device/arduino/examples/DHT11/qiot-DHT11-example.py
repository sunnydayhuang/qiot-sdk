import time
import sys  
#import websocket
import datetime
import paho.mqtt.client as mqtt
import json
import os
import ssl, socket

sys.path.insert(0, '/usr/lib/python2.7/bridge/') 
from bridgeclient import BridgeClient as bridgeclient

bridge_client = bridgeclient()
topic_pub = "com/qnap/rajah/pot"

global HOST, PORT, USER_NAME, USER_PASS, CLIENT_ID, PRIVATE_CERT, CLIENT_CERT, CA_CERT, RES_DATA
vals = ""

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

with open('./res/resourceinfo.json', 'r') as f:
	data = f.read()
	RES_DATA = json.loads(data)
	HOST = str(RES_DATA['host'][0])
	PORT = int(RES_DATA['port'])
	USER_NAME = str(RES_DATA['username'])
	USER_PASS = str(RES_DATA['password'])
	CLIENT_ID = str(RES_DATA['clientId'])
f.close()

print "HOST : " + HOST
print "PORT : " + str(PORT)
#deal with file path
arr_KEY = RES_DATA['privateCert'].split('/')
if (len(arr_KEY) > 0) :
	PRIVATE_CERT = os.getcwd() + "/ssl/" + arr_KEY[len(arr_KEY)-1]

arr_CERT = RES_DATA['clientCert'].split('/')
if (len(arr_CERT) > 0) :
	CLIENT_CERT = os.getcwd() + "/ssl/" + arr_CERT[len(arr_CERT)-1]

arr_CA = RES_DATA['caCert'].split('/')
if (len(arr_CA) > 0) :
	CA_CERT = os.getcwd() + "/ssl/" + arr_CA[len(arr_CA)-1]
# f.close()
print "CLIENT_CERT path :" + str(CLIENT_CERT)

print "PRIVATE_CERT exists or not :" + str(os.path.exists(PRIVATE_CERT))

client = mqtt.Client(client_id=CLIENT_ID)
client.on_connect = on_connect
client.on_message = on_message

if not CA_CERT :
# client.tls_set(ca_certs=str(CA_CERT), certfile=str(CLIENT_CERT), keyfile=str(PRIVATE_CERT), tls_version=ssl.SSLv23)
	client.tls_set(ca_certs=str(CA_CERT), certfile=str(CLIENT_CERT), keyfile=str(PRIVATE_CERT), cert_reqs=ssl.CERT_REQUIRED)

print "USER_NAME : " + USER_NAME + " USER_PASS : " + USER_PASS
client.username_pw_set(username=USER_NAME, password=USER_PASS)
print "finish setup"
try:
	client.connect(host=HOST, port=PORT, keepalive=60)
	if not CA_CERT :
		print "MQTTs Server connnected." + ", IP: " + HOST + "Port: " + PORT
	else :
		print "MQTT Server connnected." + ", IP: " + HOST + "Port: " + PORT
except Exception as e:
	print "Error occurred while trying to connect to QIoT Suite broker. Reason : " + str(e.args)
	sys.exit(e)

client.loop_start()


while True:
	h0 = bridge_client.get("humidity")
	t0 = bridge_client.get("temperature")

	if not h0 :
		print "Humidity: " + h0
	else :
		h0 = 0
		print "Warning : Can not read humidity value from sensor."

	if not t0 :
		print "Temperature: " + t0
	else :
		t0 = 0
		print "Warning : Can not read temperature value from sensor."

	
	t = time.time()
	date = datetime.datetime.fromtimestamp(t).strftime('%Y%m%d%H%M%S')

	resources = RES_DATA['resources']
	for res in resources:
		if ("temperature" in str(res["resourcetypename"]).strip().lower()) :
			vals = "{\"value\":"+t0+"}"
		elif("humidity" in str(res["resourcetypename"]).strip().lower()) :
			vals = "{\"value\":"+h0+"}"
		else :
			vals = "{\"value\":0}"
		print "NOW TOPIC_NAME :" + str(res["topic"]) + " MESSAGE : " + str(vals)
		client.publish(str(res["topic"]), vals)

	time.sleep(1)